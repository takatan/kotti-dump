use std::error::Error;
use std::fs::{create_dir_all, remove_dir_all, File};
use std::io::Write;
use std::path::PathBuf;

use sea_orm::{ColumnTrait, Database, EntityTrait, ModelTrait, QueryFilter};
use serde_json::Value;

use crate::entities::blobs;
use crate::entities::prelude::{Blobs, Contents, Documents, Files, Images, Nodes};

mod entities;

const OUT: &str = "target/out";
#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    // tracing_subscriber::fmt()
    //     .with_max_level(tracing::Level::DEBUG)
    //     .with_test_writer()
    //     .init();

    let url = "sqlite:///home/takaki/var/Kotti.db";
    let conn = Database::connect(url).await?;

    // let node = Nodes::find_by_id(1).one(&conn).await?.unwrap();
    // println!("{:?}", node);
    // let content = node.find_related(Contents).all(&conn).await?;
    // println!("{:?}", content);

    remove_dir_all(OUT).unwrap();
    let images = Images::find().all(&conn).await?;
    let image_dir = PathBuf::from(OUT).join("images");
    create_dir_all(&image_dir).unwrap();
    for i in images {
        let f = i.find_related(Files).one(&conn).await?.unwrap();
        let c = f.find_related(Contents).one(&conn).await?.unwrap();
        let n = c.find_related(Nodes).one(&conn).await?.unwrap();
        let d: Value = serde_json::from_str(&f.data.unwrap()).unwrap();
        let file_id = d["file_id"].as_str().unwrap();
        let b = Blobs::find()
            .filter(blobs::Column::FileId.eq(file_id))
            .one(&conn)
            .await?
            .unwrap();
        let _v = (
            i.id,
            f.filename.as_ref().unwrap(),
            file_id,
            c.description.unwrap(),
            n.name,
            n.title.unwrap(),
            n.path.unwrap(),
            b.data.as_ref().unwrap().len(),
        );
        let image_file = image_dir.join(f.filename.as_ref().unwrap());
        let mut fp = File::create(image_file).unwrap();
        fp.write_all(&b.data.unwrap()).unwrap();
        // println!("{:?}", v);
    }
    // println!("----");
    let documents = Documents::find().all(&conn).await?;
    for d in documents {
        let c = d.find_related(Contents).one(&conn).await?.unwrap();
        let n = c.find_related(Nodes).one(&conn).await?.unwrap();
        let _v = (
            n.title.as_ref().unwrap(),
            n.path.as_ref().unwrap(),
            c.default_view.as_ref(),
            c.description.as_ref().unwrap(),
            c.creation_date.as_ref().unwrap(),
            c.modification_date.as_ref().unwrap(),
            &d.body
                .as_ref()
                .unwrap()
                .chars()
                .take(10)
                .collect::<String>(),
        );
        // println!("{:?}", v);

        let adoc = format!(
            r"# {}
created_at:: {}
modified_at:: {}

{}

++++
{}
++++
",
            n.title.unwrap(),
            c.creation_date.as_ref().unwrap(),
            c.modification_date.as_ref().unwrap(),
            c.description.as_ref().unwrap(),
            &d.body.as_ref().unwrap()
        );

        let p = if c.default_view.is_some() {
            PathBuf::from(n.path.as_ref().unwrap()).join("index.adoc")
        } else {
            let p = PathBuf::from(n.path.as_ref().unwrap());
            let p = p.parent().unwrap();
            p.join(format!("{}_{}.adoc", n.position.unwrap(), &n.name))
        };

        let document_file = PathBuf::from(OUT).join(p.strip_prefix("/").unwrap());
        create_dir_all(document_file.parent().unwrap()).unwrap();
        // println!("{:?}", document_file);
        let mut fp = File::create(document_file).unwrap();
        fp.write_all(adoc.as_bytes()).unwrap();
        // println!("{}", adoc);
    }

    Ok(())
}
